# ESMeyers.org

Designed and developed by Alexander Yuen <koostudios@gmail.com> (http://www.koostudios.com).

## Changelog

### August 25, 2014
* First iteration of the website for 2014 E.S. Meyers Memorial Lecture delivered by Dr Mark Loane
* Introduction of the E.S. Meyers Memorial Lecture and "Reflections on 20 Years of Remote Eye Health" from poster
* RSVP Form to Brace orms
* Google Map
* About Professor Errol Solomer Meyers
* Implemented timeline
* About UQMS
